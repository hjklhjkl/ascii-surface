#include <stdbool.h>
#include <math.h>
#include <stdio.h>
#include <string.h>
#include <sys/ioctl.h>
#include <unistd.h>

#define TWO_PI 6.28

struct winsize w;

struct Point {
  float x;
  float y;
  float z;
};

void draw_frame(char data[])
{
  printf("\x1b[H");
  for (int i = 0; w.ws_row * w.ws_col + 1 > i; i++)
    putchar(i % w.ws_col ? data[i] : '\n');
}

void clear_screen()
{
  printf("\x1b[2J");
}

void rotate(struct Point *point, struct Point *normal, float alpha, float beta)
{
  float x = point->x;
  float y = point->y;
  const float ca = cos(alpha);
  const float cb = cos(beta);
  const float sa = sin(alpha);
  const float sb = sin(beta);
  point->x = cb * x - ca * sb * y + sa * sb * point->z;
  point->y = sb * x + ca * cb * y - sa * cb * point->z;
  point->z = sa * y + ca * point->z;

  x = normal->x;
  y = normal->y;
  normal->x = cb * x - ca * sb * y + sa * sb * normal->z;
  normal->y = sb * x + ca * cb * y - sa * cb * normal->z;
  normal->z = sa * y + ca * normal->z;
}

void add_point_to_frame(float z_buffer[], char frame[], struct Point *p,
                        struct Point *n, float distance)
{
  const char *illumination_dict = ".,-~:;=!*#$@";
  const float k1 = 40;
  // move away from viewpoint
  p->z += distance;

  int x_proj = w.ws_col / 2 + k1 * p->x / (distance + p->z);
  int y_proj = w.ws_row / 2 - k1 * p->y / (distance + p->z);

  int buffer_index = x_proj + w.ws_col * y_proj;
  const bool on_screen = w.ws_row > y_proj && y_proj > 0
      && x_proj > 0 && w.ws_col > x_proj;
  const float ooz = 1 / p->z;
  if (on_screen && ooz > z_buffer[buffer_index]) {
    const int lum = 8 * fabs(n->y - n->z);
    z_buffer[buffer_index] = ooz;
    frame[buffer_index] = illumination_dict[lum];
  }
}

void moebius_frame(float *z_buffer, char *frame, float alpha, float beta)
{
  const float r1 = 1;
  const float r2 = 3;
  for (float phi = 0; phi < TWO_PI; phi += 0.02) {
    const float sp = sin(phi);
    const float cp = cos(phi);
    const float cph = cos(phi / 2);
    const float sph = sin(phi / 2);
    for (float l = -r1; l < r1; l += r1 / 5) {
      const float g = r2 + l * cph;
      struct Point p;
      p.x = cp * g;
      p.y = sp * g;
      p.z = -l * sph;

      struct Point n;
      n.x = sph;
      n.y = 0;
      n.z = cph;

      rotate(&p, &n, alpha, beta);
      add_point_to_frame(z_buffer, frame, &p, &n, 5);
    }
  }
}

void moebius_ride_frame(float *z_buffer, char *frame, float alpha, float beta)
{
  const float r1 = 1;
  const float r2 = 3;
  const float heigt_above_strip = 0.1;
  const float ca = cos(-alpha);
  const float sa = sin(-alpha);
  const float cah = cos(alpha / 2);
  const float sah = sin(alpha / 2);
  for (float phi = 0; phi < TWO_PI; phi += 0.005) {
    const float sp = sin(phi);
    const float cp = cos(phi);
    const float cph = cos(phi / 2);
    const float sph = sin(phi / 2);
    for (float l = -r1; l < r1; l += r1 / 25) {
      const float g = r2 + l * cph;
      struct Point p;
      const float r = g * (ca * cp - sa * sp) - r2;

      p.x = cah * r + sah * l * sph;
      p.y = l * cah * sph - sah * r - heigt_above_strip;
      p.z = g * (sa * cp + ca * sp);

      struct Point n;
      n.x = sph;
      n.y = 0;
      n.z = cph;

      add_point_to_frame(z_buffer, frame, &p, &n, 0);
    }
  }
}

void torus_frame(float *z_buffer, char *frame, float alpha, float beta)
{
  const float r1 = 1;
  const float r2 = 3;
  for (float phi = 0; phi < TWO_PI; phi += 0.02) {
    const float sp = sin(phi);
    const float cp = cos(phi);
    for (float theta = 0; theta < TWO_PI; theta += 0.07) {
      const float st = sin(theta);
      const float ct = cos(theta);
      const float g = r2 + r1 * ct;
      struct Point p;
      p.x = cp * g;
      p.y = r1 * st;
      p.z = -sp * g;

      struct Point n;
      n.x = cp * ct;
      n.y = st;
      n.z = -sp * ct;

      rotate(&p, &n, alpha, beta);
      add_point_to_frame(z_buffer, frame, &p, &n, 5.5);
    }
  }
}

void cube_frame(float *z_buffer, char *frame, float alpha, float beta)
{
  const float edge_length = 5;
  const float e = edge_length / 2;
  for (int i = 0; i < 3; ++i) {
    for (float a = -e; a < e; a += 0.1) {
      for (float b = -e; b < e; b += 0.1) {
        for (int k = -1; k < 2; k += 2) {
          float pa[3];
          pa[i] = k * e;
          pa[(i + 1) % 3] = a;
          pa[(i + 2) % 3] = b;
          struct Point p;
          p.x = pa[0];
          p.y = pa[1];
          p.z = pa[2];
          float na[3] = {0, 0, 0};
          na[i] = k;
          struct Point n;
          n.x = na[0];
          n.y = na[1];
          n.z = na[2];
          rotate(&p, &n, alpha, beta);
          add_point_to_frame(z_buffer, frame, &p, &n, 6);
        }
      }
    }
  }
}

int main(int argc, char *argv[])
{
  ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
  void (*surface_frame)(float*, char*, float, float) = &moebius_frame;
  if (argc == 2) {
    if (strcmp(argv[1], "-t") == 0)
      surface_frame = &torus_frame;
    else if (strcmp(argv[1], "-c") == 0)
      surface_frame = &cube_frame;
    else if (strcmp(argv[1], "-r") == 0)
      surface_frame = &moebius_ride_frame;
  }
  float alpha = 0;
  float beta = 0;
  const int buffer_size = w.ws_col * w.ws_row;
  float z_buffer[buffer_size];
  char frame[buffer_size];
  clear_screen();
  for (;;) {
    memset(frame, ' ', buffer_size);
    memset(z_buffer, 0, sizeof(float) * buffer_size);
    (*surface_frame)(&z_buffer, &frame, alpha, beta);
    draw_frame(frame);
    // propagate rotation
    alpha += 0.004;
    beta += 0.002;
  }
  return 0;
}
