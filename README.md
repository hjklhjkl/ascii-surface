# ASCII Surfaces

```
                     !!***##
                  ==!!!**###$$
               ::;;=!!**###$$$$$
              ~~:;;=!!*##$$$$$$$$
            ,--~::;=!!*#$$$$$$$$$
           .,,--~:;=!!* $$$$$$$$$$
          ...,,--:;;    $$$$$$$$$$
         ,....,,-~     $$$$$$$$$$$
         -,,...,,     $$$$$$$$$$$$
        ~--,,...    $$$$$$$$$$$$$$
        :~~-,,.   ###$$$$$$$$$$$$
         ;;--, *######$$$$$$$$$$$
         ;=!!****#####$$$$$$$$$$
          =!!!***######$$$$$$$$$
           =!!***#######$$$$$$$
           =!!***#######$$$$$$
            =!!***######$$$$$
            =!!***#######$$$
             !!***#######$$
              !****######
                ***###
```

Idea and most of the implementation stolen from [a1kon.net](https://www.a1k0n.net/2006/09/15/obfuscated-c-donut.html)

## Build

`gcc -o surface surface.c -lm`

## Run

* To draw a Möbius strip run `./surface`
* To draw a torus run `./surface -t`
* To draw a cube run `./surface -c`
* To ride on the Möbius strip run `./surface -r`
